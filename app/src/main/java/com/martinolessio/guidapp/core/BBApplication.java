package com.martinolessio.guidapp.core;

import android.app.Application;

import com.martinolessio.guidapp.BBHelper;

/**
 * Created by martinolessio on 09/05/16.
 */
public class BBApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        BBHelper.sharedHelper().parseJson(this);

    }
}
