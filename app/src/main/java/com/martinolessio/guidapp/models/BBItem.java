
package com.martinolessio.guidapp.models;

import java.util.HashMap;
import java.util.Map;

public class BBItem {

    private String title;
    private String image;
    private String audio;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BBItem() {
    }

    /**
     * 
     * @param title
     * @param audio
     * @param image
     */
    public BBItem(String title, String image, String audio) {
        this.title = title;
        this.image = image;
        this.audio = audio;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public BBItem withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public BBItem withImage(String image) {
        this.image = image;
        return this;
    }

    /**
     * 
     * @return
     *     The audio
     */
    public String getAudio() {
        return audio;
    }

    /**
     * 
     * @param audio
     *     The audio
     */
    public void setAudio(String audio) {
        this.audio = audio;
    }

    public BBItem withAudio(String audio) {
        this.audio = audio;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BBItem withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
