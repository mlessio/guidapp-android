
package com.martinolessio.guidapp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BBContainer {

    private String title;
    private String image;
    private String audio;
    private List<BBItem> BBItems = new ArrayList<BBItem>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BBContainer() {
    }

    /**
     * 
     * @param title
     * @param audio
     * @param BBItems
     * @param image
     */
    public BBContainer(String title, String image, String audio, List<BBItem> BBItems) {
        this.title = title;
        this.image = image;
        this.audio = audio;
        this.BBItems = BBItems;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public BBContainer withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public BBContainer withImage(String image) {
        this.image = image;
        return this;
    }

    /**
     * 
     * @return
     *     The audio
     */
    public String getAudio() {
        return audio;
    }

    /**
     * 
     * @param audio
     *     The audio
     */
    public void setAudio(String audio) {
        this.audio = audio;
    }

    public BBContainer withAudio(String audio) {
        this.audio = audio;
        return this;
    }

    /**
     * 
     * @return
     *     The items
     */
    public List<BBItem> getBBItems() {
        return BBItems;
    }

    /**
     * 
     * @param BBItems
     *     The items
     */
    public void setBBItems(List<BBItem> BBItems) {
        this.BBItems = BBItems;
    }

    public BBContainer withItems(List<BBItem> BBItems) {
        this.BBItems = BBItems;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BBContainer withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
