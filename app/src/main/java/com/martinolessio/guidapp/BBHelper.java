package com.martinolessio.guidapp;

import android.content.Context;

import com.martinolessio.guidapp.models.BBContainer;
import com.martinolessio.guidapp.models.BBItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by martinolessio on 21/03/16.
 */
public class BBHelper {

    private static final BBHelper _helper = new BBHelper();

    private BBContainer mActiveRoom;
    private BBItem mActiveItem;

    private List<BBContainer> mItems = new ArrayList<>();

    public static BBHelper sharedHelper(){
        return _helper;
    }

    public BBContainer getActiveRoom(){
        return mActiveRoom;
    }

    public BBItem getActiveItem(){
        return mActiveItem;
    }

    public void setActiveRoom(BBContainer theItem){
        mActiveRoom = theItem;
    }

    public void setActiveItem(BBItem theItem){
        mActiveItem = theItem;
    }

    public List<BBContainer> getItems() {
        return mItems;
    }

    public void setItems(List<BBContainer> mItems) {
        this.mItems = mItems;
    }

    public void parseJson(Context ctx){
            String json = null;
            try {

                InputStream is = ctx.getResources().openRawResource(R.raw.guidapp);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");

                JSONArray array = new JSONArray(json);

                List<BBContainer> containers = new ArrayList<>();

                for(int i = 0; i < array.length(); i++){
                    JSONObject jsonObject = array.getJSONObject(i);

                    String title = jsonObject.getString("title");
                    String image = jsonObject.getString("image");
                    String audio = jsonObject.getString("audio");

                    JSONArray items = jsonObject.getJSONArray("items");

                    List<BBItem> newItems = new ArrayList<>();

                    for(int j = 0; j < items.length(); j++){
                        JSONObject itemJson = items.getJSONObject(j);

                        String iTitle = itemJson.getString("title");
                        String iImage = itemJson.getString("image");
                        String iAudio = itemJson.getString("audio");

                        newItems.add(new BBItem(iTitle,iImage,iAudio));
                    }

                    containers.add(new BBContainer(title,image,audio,newItems));
                }

                mItems = containers;

            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


    }
}
