package com.martinolessio.guidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import com.martinolessio.guidapp.models.BBItem;

public class BBRoomActivity extends AppCompatActivity {

    private ViewGroup mScrollContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbroom);

        mScrollContent = ((ViewGroup) findViewById(R.id.scrollContent));

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        if(BBHelper.sharedHelper().getActiveRoom() == null){
            Intent intent = new Intent(this, BBHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        buildView();
    }

    private void buildView(){

        int rows = BBHelper.sharedHelper().getActiveRoom().getBBItems().size() / 2 + BBHelper.sharedHelper().getActiveRoom().getBBItems().size() % 2;
        int rowHeight = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, getResources().getDisplayMetrics()));
        int padding = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics()));

        for (int i = 0; i < rows; i++){
            final BBItem leftItem = BBHelper.sharedHelper().getActiveRoom().getBBItems().get(i * 2);

            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight));

            Button leftTile = new Button(this);
            leftTile.setText(leftItem.getTitle());
            leftTile.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));

            row.setPadding(padding,padding,padding,padding);

            leftTile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemSelected(leftItem);
                }
            });

            leftTile.setContentDescription(leftItem.getTitle());

            row.addView(leftTile);

            if(BBHelper.sharedHelper().getActiveRoom().getBBItems().size() > (i*2 + 1)) {
                final BBItem rightItem = BBHelper.sharedHelper().getActiveRoom().getBBItems().get(i * 2 + 1);

                Button rightTile = new Button(this);
                rightTile.setText(rightItem.getTitle());

                rightTile.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));

                rightTile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemSelected(rightItem);
                    }
                });

                rightTile.setContentDescription(rightItem.getTitle());

                row.addView(rightTile);

            }else{
                LinearLayout placeholderView = new LinearLayout(this);
                placeholderView.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));
                row.addView(placeholderView);
            }

            mScrollContent.addView(row);

        }

        LinearLayout row = new LinearLayout(this);
        row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        Button backButton = new Button(this);
        backButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight));

        backButton.setText(R.string.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        backButton.setContentDescription(getString(R.string.back));

        row.setPadding(padding,padding,padding,padding);
        row.addView(backButton);
        mScrollContent.addView(row);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(BBHelper.sharedHelper().getActiveRoom() == null){
            Intent intent = new Intent(this, BBHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }
    }

    /*
    * Item Selected
    * */
    public void itemSelected(BBItem theItem)
    {
        Intent itemIntent = new Intent(this, BBItemActivity.class);
        BBHelper.sharedHelper().setActiveItem(theItem);
        startActivity(itemIntent);
    }

    /*
    * Back selected
    * */
    public void backSelected(View v)
    {
        onBackPressed();
    }
}
