package com.martinolessio.guidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.martinolessio.guidapp.models.BBContainer;

public class BBHomeActivity extends AppCompatActivity {

    private ViewGroup mScrollContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbhome);

        mScrollContent = ((ViewGroup) findViewById(R.id.scrollContent));

        getSupportActionBar().setTitle(R.string.home_label);
        setTitle(R.string.home_label);

        if(BBHelper.sharedHelper().getItems() == null)
            BBHelper.sharedHelper().parseJson(this);

        buildView();

    }

    private void buildView(){

        int rows = BBHelper.sharedHelper().getItems().size() / 2 + BBHelper.sharedHelper().getItems().size() % 2;

        Log.d("debug", "rows: " + rows);

        int rowHeight = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, getResources().getDisplayMetrics()));

        for (int i = 0; i < rows; i++){
            final BBContainer leftItem = BBHelper.sharedHelper().getItems().get(i * 2);

            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            View leftTile = getLayoutInflater().inflate(R.layout.item_tile, row, false);
            ImageView leftImage = ((ImageView) leftTile.findViewById(R.id.imageView));
            TextView leftText = ((TextView) leftTile.findViewById(R.id.textView));

            leftText.setText(leftItem.getTitle());

            leftTile.setContentDescription(leftItem.getTitle());

            int leftRId = getResources().getIdentifier("thumb_" + leftItem.getImage().replace(".jpg", ""), "raw", getPackageName());
            if(leftRId > 0)
                leftImage.setImageDrawable(getResources().getDrawable(leftRId));

            leftTile.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight, 1));

            leftTile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    roomSelected(leftItem);
                }
            });

            row.addView(leftTile);

            if(BBHelper.sharedHelper().getItems().size() > (i*2 + 1)) {
                final BBContainer rightItem = BBHelper.sharedHelper().getItems().get(i * 2 + 1);

                View rightTile = getLayoutInflater().inflate(R.layout.item_tile, row, false);
                ImageView rightImage = ((ImageView) rightTile.findViewById(R.id.imageView));
                TextView rightText = ((TextView) rightTile.findViewById(R.id.textView));

                rightText.setText(rightItem.getTitle());

                int rightRId = getResources().getIdentifier("thumb_" + rightItem.getImage().replace(".jpg", ""), "raw", getPackageName());
                if(rightRId > 0)
                    rightImage.setImageDrawable(getResources().getDrawable(rightRId));
                rightTile.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight, 1));

                rightTile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        roomSelected(rightItem);
                    }
                });

                rightTile.setContentDescription(rightItem.getTitle());

                row.addView(rightTile);

            }else{
                LinearLayout placeholderView = new LinearLayout(this);
                placeholderView.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight, 1));
                row.addView(placeholderView);
            }

            mScrollContent.addView(row);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(BBHelper.sharedHelper().getItems() == null){
            BBHelper.sharedHelper().parseJson(this);

            mScrollContent.removeAllViews();
            buildView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    /*
    * Button press
    * */
    public void roomSelected(BBContainer theItem)
    {
        Intent roomIntent = new Intent(this, BBRoomActivity.class);
        BBHelper.sharedHelper().setActiveRoom(theItem);
        startActivity(roomIntent);
    }

    public void infoPressed(MenuItem item){
        Intent aboutActivity = new Intent(this, BBAboutActivity.class);
        startActivity(aboutActivity);
    }
}
