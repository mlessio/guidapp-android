package com.martinolessio.guidapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class BBItemActivity extends AppCompatActivity {

    private MediaPlayer mPlayer;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbitem);

        if(BBHelper.sharedHelper().getActiveItem() == null){
            Intent intent = new Intent(this, BBHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

            return;
        }

        mImageView = ((ImageView) findViewById(R.id.imageView));

        mImageView.setContentDescription(BBHelper.sharedHelper().getActiveItem().getTitle());

        int rightRId = getResources().getIdentifier("img_" + BBHelper.sharedHelper().getActiveItem().getImage().replace(".jpg", ""), "raw", getPackageName());
        if(rightRId > 0)
            mImageView.setImageDrawable(getResources().getDrawable(rightRId));

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        int fileIdentifier = getResources().getIdentifier("snd_" + BBHelper.sharedHelper().getActiveItem().getAudio().replace(".mp3", ""),
                "raw", getPackageName());

        if (fileIdentifier > 0)
            mPlayer = MediaPlayer.create(this, fileIdentifier);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(BBHelper.sharedHelper().getActiveItem() == null){
            Intent intent = new Intent(this, BBHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (null != mPlayer) {
            mPlayer.stop();
            mPlayer.reset();
            mPlayer.release();
        }
        super.onBackPressed();
    }


    /*
        * Play Selected
        * */
    public void playSelected(View v) {
        if (null != mPlayer) {
            if (mPlayer.isPlaying())
                mPlayer.pause();
            else
                mPlayer.start();
        }
    }

    /*
    * Pause selected
    * */
    public void pauseSelected(View v) {
        if (null != mPlayer) {
            mPlayer.pause();
        }
    }

    /*
    * Back selected
    * */
    public void backSelected(View v) {
        onBackPressed();
    }

}
